// The following *-min task will produce minified files in the dist folder
// By default, your `index.html`'s <!-- Usemin block --> will take care of
// minification. These next options are pre-configured if you do not wish
// to use the Usemin blocks.
// Also make sure do uncomment the configuration in cssmin.js and uglify.js
module.exports = {
//   dist: {}
};
